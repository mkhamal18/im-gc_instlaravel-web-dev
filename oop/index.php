<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OOP PHP</title>
</head>
<body>

    <h1>Berikut merupakan latihan OOP PHP</h1>

    <?php

    include('animal.php');
    include('frog.php');
    include('ape.php');


    echo"<h3>Sheep</h3>";
    $sheep = new Animal("Shaun");
    echo"<br>";
    echo"Name :";
    echo $sheep->name;
    echo"<br>";
    echo"Legs :";
    echo $sheep->legs;
    echo"<br>";
    echo"Cold Blooded";
    echo $sheep->cold_blooded;

    echo"<br><br><br><h3> Kodok</h3>";
    $kodok = new Frog("Buduk");
    echo"<br>";
    echo"Name :";
    echo $kodok->name;
    echo"<br>";
    echo"Legs :";
    echo $kodok->legs;
    echo"<br>";
    echo"Cold Blooded :";
    echo $kodok->cold_blooded;
    echo"<br>";
    echo"Jump :";
    echo $kodok->jump;

    echo"<br><br><br><h3> Kera</h3>";
    $sungokong = new Ape("Kera Sakti");
    echo"<br>";
    echo"Name :";
    echo $sungokong->name;
    echo"<br>";
    echo"Legs :";
    echo $sungokong->legs;
    echo"<br>";
    echo"Cold Blooded :";
    echo $sungokong->cold_blooded;
    echo"<br>";
    echo"Yell :";
    echo $sungokong->yell;

?>

</body>
</html>