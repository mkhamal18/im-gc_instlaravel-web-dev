<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Function</title>
</head>
<body>
    <h1>Berlatih Function PHP</h1>

    <?php
        echo"<h3> Soal No 1 Greetings </h3>";

        function greetings($nama) {
            echo"Halo $nama, Selamat Datang Di Garuda Cyber Institute!";
            return;

        };

        echo greetings("Bagas");
        echo"<br>";
        echo greetings("Wahyu");
        echo"<br>";
        echo greetings("Muharfan Khamal");

        echo"<h3> Soal No 2 Reverse String </h3>";

        function reverseString($reStr) {
            $pjg = strlen($reStr);
            for($i =($pjg-1); $i >=0; $i--) {
                echo $reStr[$i];
            }
        };

        echo reverseString("Muharfan Khamal");
        echo"<br>";
        echo reverseString("Garuda Cyber Institute");
        echo"<br>";
        echo reverseString("We Are GC-Ins Developer");

        echo"<h3> Soal No 3 Palindrome</h3>";

        // function palindrome($palin) {
        //     $length = strlen($palin);
        //     for($j = 0; $j < $length / 2; $j--) {
        //         if ($palin[$j] !== $palin[$length - $j - 1]) {
        //             return false;
        //         }
        //     }
        //     return true;
        // };

        
        function palindrome($palin) {
            $balik='';
            $panjang = strlen($palin);

            for($j =($panjang-1); $j >=0; $j--) {
                $balik.=$palin[$j];
            }
                if ($palin == $balik) {
                    echo "True";
                } else {
                    echo"False";
                }
        };
        
        echo("Apakah civic merupakan palindrome? =");
        echo palindrome("civic");
        echo"<br>";
        echo("Apakah nababan merupakan palindrome? =");
        echo palindrome("nababan");
        echo"<br>";
        echo("Apakah jambaban merupakan palindrome? =");
        echo palindrome("jambaban");
        echo"<br>";
        echo("Apakah racecar merupakan palindrome? =");
        echo palindrome("racecar");


        echo"<h3> Soal No 4 Temukan Nilai </h3>";
        function tentukan_nilai($nilai) {
            if ($nilai >=85 and $nilai <=100) {
                echo"Sangat Baik";
            } elseif ($nilai >=70 and $nilai <=85) {
                echo"Baik";
            } elseif ($nilai >=60 and $nilai <=70) {
                echo"Cukup";
            } else {
                echo"Kurang";
            }
        };

        echo"Anda Mendapatkan Nilai 98 <br>";
        echo"Grade Anda: ";
        echo tentukan_nilai(98);
        echo"<br><br>";
        echo"Anda Mendapatkan Nilai 76 <br>";
        echo"Grade Anda: ";
        echo tentukan_nilai(76);
        echo"<br><br>";
        echo"Anda Mendapatkan Nilai 67 <br>";
        echo"Grade Anda: ";
        echo tentukan_nilai(67);
        echo"<br><br>";
        echo"Anda Mendapatkan Nilai 43 <br>";
        echo"Grade Anda: ";
        echo tentukan_nilai(43);
    ?>
</body>
</html>