<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Looping</title>
</head>
<body>
    <h1>Berlatih Looping</h1>

    <?php

    echo "<h3> Soal No 1 Looping I love PHP</h3>";
    echo"<br>";
    echo"LOOPING PERTAMA:<br><br>";
    for($i = 2; $i <=20; $i++) {
        if($i % 2 == 0) {
            echo"$i - I Love PHP <br>";
        }
    }

    echo"<br><br>LOOPING KEDUA:<br><br>";
    for($j =20; $j >=2; --$j) {
        if($j % 2 == 0) {
            echo"$j - I Love PHP <br>";
        }
    }


    echo"<br><br>Soal No 2 Looping Array module";
    echo"<br>";

    $numbers = [18, 45, 29, 61, 47, 34];
    echo"array numbers<br><br>";
    print_r($numbers);

    foreach($numbers as & $numbers) {
        $rest = $numbers % 5;
        echo"<br><br>Array sisa baginya adalah: $rest <br>";
    }


    echo"<br><br>Soal No 3 Loop Array Associative<br><br>";

    $items = [
        ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'],
        ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
        ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
        ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
        ];

    foreach($items as $item) {
        $itemData = [
            'id' => $item[0],
            'name' => $item[1],
            'price' => $item[2],
            'description' => $item[3],
            'source' => $item[4]
        ];

        print_r($itemData);
        echo"<br>";
    }
    
    echo"<br><br>";
    echo" Soal No 4 Asterisk Loop";
    echo"<br><br>";
   echo"Asterisks<br><br>";
    for($i=1; $i<=5; $i++) {
        for($j=1; $j<=$i; $j++){
        echo"*";
        }
        echo"<br>";
    }

    ?>
</body>
</html>